import React from 'react';
import { render } from '@testing-library/react';
import ReactTestUtils from 'react-dom/test-utils'; 
import Player from './Player';

let sut = null;

beforeEach(() => {
  sut = new Player('X')
});

test('Player name is value in constructor', () => {
  expect(sut.name).toBe('X');
});

test('Player hasWon is false', () => {
  expect(sut.hasWon).toBe(false);
});

test('Player currentlyPlaying is false', () => {
  expect(sut.currentlyPlaying).toBe(false);
});

test('Player cursor is default', () => {
  expect(sut.cursor).toBe('default');
});