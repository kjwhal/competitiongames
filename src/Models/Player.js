class Player {
    constructor(name) {
      this.name = name;
      this.hasWon = false;
      this.currentlyPlaying = false;
      this.cursor = 'default';
      this.symbol = name[0];
    }
  }

  export default Player