import React from 'react';
import TicTacToe from './TicTacToe'
import './TicTacToe.css'
import Player from '../Models/Player';

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      players: Array(0),
      cursor: 'default'
    };

    this.state.players.push(new Player('Kevin'));
    this.state.players.push(new Player('Joe'));
  }

  completeTurn() {
    const players = this.state.players.slice();

    var winner = players.find(p => p.hasWon === true);
      if (winner) {
        this.notifyWinner(winner);

        players.forEach(p => p.cursor = 'gameWon');

        this.setState({players: players});
        this.togglePlayers();

        return;
      }

      this.togglePlayers();
  }

  togglePlayers() {
    const players = this.state.players.slice();

    const player = players.shift();

    player.currentlyPlaying = false;

    players.push(player);

    players[0].currentlyPlaying = true;

    this.setState({players: players});
  }

  notifyWinner(winner) {
    alert('Winner: ' + winner.name);
  }

  movesAllowed() {
    const players = this.state.players.slice();

    const gameOver = players.filter((p) => { return p.hasWon}).length > 0;

    if(gameOver) {
      alert('Game ended.');
    }

    return !gameOver;
  }

  reset() {
    const players = this.state.players.slice();

    players.forEach(player => {
      player.hasWon = false;
      player.cursor = 'default';
    });

    this.setState({players: players, foo: true});
  }

  render() {
    return (
      <div className="game">
        <div className="game-board">
          <TicTacToe value={this.state}
            completeTurn={this.completeTurn.bind(this)}
            movesAllowed={this.movesAllowed}
            resetGame={this.reset.bind(this)}/>
        </div>
        <div className="game-info">
        </div>
      </div>
    );
  }
}

export default Game

