import React from 'react';
import { render } from '@testing-library/react';
import ReactTestUtils from 'react-dom/test-utils'; 
import Square from './Square';
import Player from '../Models/Player';

let testOfClickEventSignal = false;

let sut = null;
let buttonElement = null;

beforeEach(() => {
  const player = new Player('Kevin');

  player.currentlyPlaying = true;

  const state = {players: [ player ]};

  sut = render(<Square value="1" state={state} onClick={() => { testOfClickEventSignal = true; }} />);

  buttonElement = sut.getByRole('button');
});

test('Renders a button', () => {
  expect(buttonElement).toBeInTheDocument();
});

test('Renders a button with class name square', () => {
  expect(buttonElement.className).toBe('square default');
});

test('Renders a button which performs passed click event', () => {
  ReactTestUtils.Simulate.click(buttonElement);
  
  expect(testOfClickEventSignal).toBe(true);
});

test('Renders a button whose text value is property: value', () => {
  expect(buttonElement.textContent).toBe("1");
});