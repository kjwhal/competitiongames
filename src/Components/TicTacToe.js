import React from 'react';
import Square from './Square';
import PlayerStatus from './PlayerStatus'

class TicTacToe extends React.Component {
    constructor(props) {
      super(props);

      this.completeTurn = this.props.completeTurn;
      this.movesAllowed = this.props.movesAllowed;
      
      this.resetGame = this.props.resetGame;

      this.state = {
        players: this.props.value.players,
        squares: Array(9).fill(null),
        cursor: this.props.value.cursor
      };

      const players = this.state.players.slice();

      players[0].symbol = 'X';
      players[1].symbol = 'O';

      var currentPlayer = players.find(player => player.currentlyPlaying === true);

      if(!currentPlayer) {
        players[0].currentlyPlaying = true;
      }
    }

    renderSquare(squarePosition) {
      return (
        <Square
          value={this.state.squares[squarePosition]}
          state={this.state}
          onClick={() => this.playerMove(squarePosition)}
        />
      );
    }
  
    playerMove(squarePosition) {
      if(this.movesAllowed() === false) {
        return;
      }
  
      const squares = this.state.squares.slice();
  
      if(squares[squarePosition]) {
        alert('Cannot be played twice!');

        return;
      }

      const players = this.state.players.slice();
  
      const player = players.find(p => p.currentlyPlaying);
  
      squares[squarePosition] = player.symbol;
      
      this.setState({squares: squares});

      this.determineWinner(squares);

      this.completeTurn();
    }

    determineWinner(squares) {
      const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
      ];
  
      const players = this.state.players;

      for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
          const currentPlayer = players.find(p => p.currentlyPlaying);
          currentPlayer.hasWon = true;
        }
      }

      this.setState({players:players});
    }

    reset() {
      this.resetGame();
      this.setState({squares: Array(9).fill(null)});
    }
    
    render() {
      return (
        <div>
        <PlayerStatus value={this.state}/>
        <div>
          <button onClick={this.reset.bind(this)}>Reset</button>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
        </div>
      );
    }
  }

export default TicTacToe