import React from 'react';

class PlayerStatus extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        players: this.props.value.players
      };
    }

    determineStatus(players) {
      const playerWhoWonGame = players.find(p => p.hasWon === true);

      if(playerWhoWonGame) {
        return `Player ${playerWhoWonGame.name} has won!`;
      }

      const currentPlayer = players.find(p => p.currentlyPlaying === true);

      if(currentPlayer) {
        return `Next player: ${currentPlayer.name}`;
      }

      return 'Next player: No players currently playing';
    }

    render() {
        const players = this.state.players.slice();

        const status = this.determineStatus(players);

        return (
          <div className="status">{status}</div>
        );
    }
}

export default PlayerStatus