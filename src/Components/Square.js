import React from 'react'

class Square extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      players: this.props.state.players
    };
  }

    render() {
      const currentPlayer = this.state.players.find(p => p.currentlyPlaying === true);

      return (
        <button
          className={`square ${currentPlayer.cursor}`}
          onClick={() => this.props.onClick()}
        >
          {this.props.value}
        </button>
      );
    }
  }

  export default Square