import React from 'react';
import { render } from '@testing-library/react';
import PlayerStatus from './PlayerStatus';

let sut = null;
let state = {};

beforeEach(() => {
  state = {players: []};
});

test('Renders a div with className status', () => {
  sut = render(<PlayerStatus value={state} />); 

  const matchingDiv = document.querySelector('div.status');

  expect(matchingDiv).not.toBe.null;
});

test('And has no players renders a div with status of no players currently playing', () => {
  sut = render(<PlayerStatus value={state} />); 

  const matchingDiv = document.querySelector('div.status');

  expect(matchingDiv.innerHTML).toBe('Next player: No players currently playing');
});

test('And has no winning players renders a div with status of current player', () => {
  state.players.push({ name:'Kevin', currentlyPlaying: true});
  
  sut = render(<PlayerStatus value={state} />);

  const matchingDiv = document.querySelector('div.status');

  expect(matchingDiv.innerHTML).toBe('Next player: Kevin');
});

test('And has has winning player renders a div with status of winning player', () => {
  state.players.push({ name:'Kevin', hasWon: true});
  
  sut = render(<PlayerStatus value={state} />);

  const matchingDiv = document.querySelector('div.status');

  expect(matchingDiv.innerHTML).toBe('Player Kevin has won!');
});